<?php

/**
 * @file
 * Contains Drupal\multistep8_ex\Form\MultiStepForm.
 */

namespace Drupal\multistep8_ex\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MultiStepForm.
 *
 * @package Drupal\multistep8_ex\Form
 */
class MultiStepForm extends ConfigFormBase
{

  protected $step = 1;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'multistep8_ex.multistep',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'multi_step_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('multistep8_ex.multistep');

    $model_ops = array(
      '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004',
      '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012',
      '2013', '2014', '2015');

    $body_style_ops = array('Coupe', 'Sedan', 'Convertible', 'Hatchback',
      'Station wagon', 'SUV', 'Minivan', 'Full-size van', 'Pick-up');

    $gas_mileage_ops = array('20 mpg or less', '21 mpg or more', '26 mpg or more',
      '31 mpg or more', '36 mpg or more', '41 mpg or more');

    switch ($this->step) {
      case 1:
        $form['model'] = [
          '#type' => 'select',
          '#title' => $this->t('Model'),
          '#description' => $this->t(''),
          '#options' => $model_ops,
//          '#default_value' => $config->get('model'),
        ];
        $form['buttons']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Next'),
        );
        break;

      case 2:
        $form['body_style'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Body Style'),
          '#description' => $this->t(''),
          '#options' => $body_style_ops,
//          '#default_value' => (array) $config->get('body_style'),
        ];
        $form['buttons']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Next'),
        );
        drupal_set_message($this->t('Previous choice:<br> <b>Model</b>: %value;', array(
          '%value' => $model_ops[$config->get('model')],
        )));
        break;

      case 3:
        $form['gas_mileage'] = [
          '#type' => 'radios',
          '#title' => $this->t('Gas Mileage'),
          '#description' => $this->t(''),
          '#options' => $gas_mileage_ops,
//          '#default_value' => $config->get('gas_mileage'),
        ];
        $form['buttons']['submit'] = array(
          '#type' => 'submit',
          '#value' => $this->t('Find a Car'),
        );
        drupal_set_message($this->t('Previous choice:<br> <b>Body Style(s)</b>: %value;', array(
          '%value' => implode(', ', array_keys(array_intersect(array_flip($body_style_ops), $config->get('body_style')))),
        )));
        break;

      case 4:
        $form['info'] = [
          '#type' => 'markup',
          '#title' => $this->t('Gas Mileage'),
          '#markup' => $this->t('Your choices:<br> <b>Model</b>: %model;<br> <b>Body Style(s)</b>: %body_style;<br> <b>Gas Mileage</b>: %gas_mileage', array(
            '%model' => $model_ops[$config->get('model')],
            '%body_style' => implode(', ', array_keys(array_intersect(array_flip($body_style_ops), $config->get('body_style')))),
            '%gas_mileage' => $gas_mileage_ops[$config->get('gas_mileage')],
          )),
        ];
        break;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    switch ($this->step) {
      case 1:
        if ($form_state->hasValue('model')) {
          $this->config('multistep8_ex.multistep')
            ->set('model', $form_state->getValue('model'))
            ->save();
        }
        $form_state->setRebuild();
        $this->step++;
        break;

      case 2:
        if ($form_state->hasValue('body_style')) {
          $this->config('multistep8_ex.multistep')
            ->set('body_style', $form_state->getValue('body_style'))
            ->save();
        }
        $form_state->setRebuild();
        $this->step++;
        break;

      case 3:
        if ($form_state->hasValue('gas_mileage')) {
          $this->config('multistep8_ex.multistep')
            ->set('gas_mileage', $form_state->getValue('gas_mileage'))
            ->save();
        }
        $form_state->setRebuild();
        $this->step++;
        parent::submitForm($form, $form_state);
        break;
    }
  }

}